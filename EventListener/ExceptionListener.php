<?php

namespace Moneyfge\BalanceBundle\EventListener;

use Moneyfge\BalanceBundle\Controller\ServiceController;
use Moneyfge\BalanceBundle\Service\ApiResponse;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerResolver;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

class ExceptionListener
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var ApiResponse
     */
    protected $apiResponse;

    /**
     * ExceptionListener constructor.
     * @param ContainerInterface $container
     * @param ApiResponse $apiResponse
     */
    public function __construct(ContainerInterface $container, ApiResponse $apiResponse)
    {
        $this->container = $container;
        $this->apiResponse = $apiResponse;
    }

    /**
     * Catching exceptions for giving correct json response.
     * Works only for instanceof ServiceController
     *
     * @param GetResponseForExceptionEvent $event
     * @throws \Exception
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {

        $controllerName = $this->container->get('controller_name_converter');
        $resolver = new ControllerResolver($this->container, $controllerName);
        $controller = $resolver->getController($event->getRequest());

        if ($controller && $controller[0] instanceof ServiceController) {

            $response = $this->apiResponse->createErrorResponse(400, [$event->getException()->getMessage()]);

            $event->setResponse($response);
        }
    }
}