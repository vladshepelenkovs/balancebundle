<?php

namespace Moneyfge\BalanceBundle\Tests\ApiPoint;

use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractApiTest extends WebTestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Initialization
     */
    public function setUp()
    {
        self::bootKernel();

        $this->container = self::$kernel->getContainer();

        $this->client = new Client([
            'base_url' => '/bb/balances'
        ]);

    }

    /**
     * Get full url by route name
     * @param string $routeName
     * @return string
     */
    protected function getUrlByRoute($routeName)
    {
        return $host = $this->container->getParameter('balance.bundle_host')
            . $this->container->get('router')->getRouteCollection()->get($routeName)->getPath();
    }


    /**
     * Checking fields of response, expected to be positive
     * @param $data
     */
    protected function checkResponseSuccess($data)
    {
        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('timestamp', $data);
        $this->assertArrayHasKey('data', $data);
    }

    /**
     * Checking fields of response, expected to be negative
     * @param $data
     */
    protected function checkResponseFailed($data)
    {
        $this->assertArrayHasKey('status', $data);
        $this->assertArrayHasKey('timestamp', $data);
        $this->assertArrayHasKey('errors', $data);
    }
}