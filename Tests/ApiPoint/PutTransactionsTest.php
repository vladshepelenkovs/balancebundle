<?php

namespace Moneyfge\BalanceBundle\Tests\ApiPoint;


use GuzzleHttp\Exception\TransferException;

class PutTransactionsTest extends AbstractApiTest
{
    /**
     * Check, if route is accessible.
     * Testing of transaction handling implemented in TransactionManagerTest test case.
     * @return void
     */
    public function testPositiveResponse()
    {
        $response = $this->client->post($this->getUrlByRoute('bb_transactions'), [
            'json' => [],
            'headers' => [
                'Authorization' => $this->container->getParameter('sso_auth.server_secret')
            ]
        ]);
        $data = json_decode($response->getBody(), true);
        $this->checkResponseSuccess($data);
    }

    /**
     * Checking response without s2s authorization.
     * @return void
     */
    public function testGetNoAuthResponse()
    {
        try {
            $response = $this->client->get($this->container->getParameter('balance.bundle_host') . '/bb/balances', []);
        } catch (TransferException $e) {
            $data = json_decode( (string)$e->getResponse()->getBody(), true);
        }

        $this->checkResponseFailed($data);
    }

}