<?php

namespace Moneyfge\BalanceBundle\Tests\ApiPoint;


use GuzzleHttp\Exception\TransferException;

class GetBalancesTest extends AbstractApiTest
{
    /**
     * Checking positive data sets
     * @return void
     */
    public function testGetPositiveResponse()
    {
        foreach ($this->getCorrectData() as $dataSet) {
            $response = $this->client->get($this->getUrlByRoute('bb_balances'), [
                'query' => $dataSet,
                'headers' => [
                    'Authorization' => $this->container->getParameter('sso_auth.server_secret')
                ]
            ]);
            $data = json_decode($response->getBody(), true);
            $this->checkResponseSuccess($data);
        }
    }

    /**
     * Checking response without s2s authorization
     * @return void
     */
    public function testGetNoAuthResponse()
    {
        try {
            $this->client->get($this->getUrlByRoute('bb_balances'), []);
        } catch (TransferException $e) {

           $data = json_decode( (string)$e->getResponse()->getBody(), true);
        }

        $this->checkResponseFailed($data);
    }

    /**
     * Checking wrong sets of data
     * @return void
     */
    public function testGetBrokenResponses()
    {
        foreach ($this->getIncorrectData() as $dataset) {
            try {
                $response = $this->client->get($this->getUrlByRoute('bb_balances'), [
                    'query' => $dataset,
                    'headers' => [
                        'Authorization' => $this->container->getParameter('sso_auth.server_secret')
                    ]
                ]);
            } catch (TransferException $e) {
                $data = json_decode( (string)$e->getResponse()->getBody(), true);
            }

            $this->checkResponseFailed($data);
        }
    }

    /**
     * Correct data sets
     * @return array
     */
    private function getCorrectData()
    {
        return [
            [
                'user_id' => rand(1, 10000)
            ],
            [
                'user_id' => rand(1, 10000),
                'balance_type' => 'card'
            ],
            [
                'user_id' => rand(1, 10000),
                'balance_type' => 'wallet',
                'currency' => ['USD','EUR']
            ]
        ];
    }

    /**
     * Incorrect data sets
     * @return array
     */
    private function getIncorrectData()
    {
        return [
            [

            ],
            [
                'user_id' => 'weirdThing'
            ],
            [
                'user_id' => rand(1, 10000),
                'this_field' => 'should not exist'
            ],
            [
                'user_id' => rand(1, 10000),
                'currency' => 'USD'
            ]
        ];
    }

}

