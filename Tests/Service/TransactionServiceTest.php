<?php

namespace Moneyfge\BalanceBundle\Tests\Service;

use Doctrine\Common\Persistence\ObjectRepository;
use GuzzleHttp\Client;
use Moneyfge\BalanceBundle\Entity\BalanceTransaction;
use Moneyfge\BalanceBundle\Service\ArrayValidator;
use Moneyfge\BalanceBundle\Service\HttpService;
use Moneyfge\BalanceBundle\Service\HttpTransport;
use Moneyfge\BalanceBundle\Service\RawService;
use Moneyfge\BalanceBundle\Service\TransactionManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Moneyfge\BalanceBundle\Service\TransactionService;

class TransactionServiceTest extends AbstractServiceTest
{
    /**
     * Check transaction process for getting positive response
     * @return void
     */
    public function testTransactionsProcess()
    {
        $transactionService = $this->getTransactionService();

        try {
            $results = $transactionService->handleTransactions($this->getTestTransactions());
        } catch (\Exception $e) {
            $results = null;
        }

        $this->assertInternalType('array', $results);

        foreach ($results as $result) {
            $this->assertArrayHasKey('transaction_id', $result);
            $this->assertInternalType('string', $result['transaction_id']);

            $this->assertArrayHasKey('result', $result);
            $this->assertInternalType('integer', $result['result']);

            $this->assertArrayHasKey('message', $result);
            $this->assertInternalType('string', $result['message']);
        }
    }

    /**
     * Checks exception with incorrect input data
     * @return void
     * @throws \Exception
     */
    public function testTransactionsProcessFailure()
    {
        $transactionService = $this->getTransactionService();
        $this->expectException(\Exception::class);
        $transactionService->handleTransactions($this->getTestTransactionsBroken());
    }

    /**
     * Adding mock repository to mock entity manager from inherited method.
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getMockEntityManager()
    {
        $mockEntityManager = parent::getMockEntityManager();

        $balanceTransactionRepository = $this->createMock(ObjectRepository::class);

        $balanceTransactionRepository->expects($this->any())
            ->method('findBy')
            ->willReturn($this->getEntitiesSet());

        $mockEntityManager->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($balanceTransactionRepository));

        return $mockEntityManager;
    }

    /**
     * Creating TransactionManager instance.
     * @return TransactionManager
     */
    protected function getTransactionWriter()
    {
        return new TransactionManager($this->getMockEntityManager());
    }

    /**
     * Returns default RawService child of BalanceService class.
     * @return RawService
     */
    protected function getBalanceService()
    {
        return new RawService(
            new ParameterBag(),
            new ArrayValidator(),
            new HttpService(new ParameterBag(), new HttpTransport( new ParameterBag(), new Client() )),
            $this->getTransactionWriter(),
            $this->getMockEntityManager()
        );
    }

    /**
     * Returns instance of TransactionService
     * @return TransactionService
     */
    protected function getTransactionService()
    {
        return new TransactionService(
            $this->getMockEntityManager(),
            $this->getBalanceService(),
            $this->getTransactionWriter(),
            new ArrayValidator()
        );
    }

    /**
     * Set of correct transaction inputs
     * @return array
     */
    protected function getTestTransactions()
    {
        return [
            0 =>
                [
                    'transaction_id' => '3d2868b6-6749-11e9-8d49-5404a696003a',
                    'type' => 1,
                    'balance_type' => 'card',
                    'balance_id' => 12,
                    'amount' => 20.56,
                    'status' => 1,
                    'user_id' => 1,
                    'currency' => 'TL',
                ],
            4 =>
                [
                    'transaction_id' => '3d2c34bb-6749-11e9-8d49-5404a696003a',
                    'type' => 2,
                    'balance_type' => 'wallet',
                    'balance_id' => 13,
                    'amount' => 36.460522427499996,
                    'status' => 1,
                    'user_id' => 1,
                    'currency' => 'KWD',
                ],
            5 =>
                [
                    'transaction_id' => '3d2d109c-6749-11e9-8d49-5404a696003a',
                    'type' => 1,
                    'balance_type' => 'card',
                    'balance_id' => 11,
                    'amount' => 67.33,
                    'status' => 1,
                    'user_id' => 1,
                    'currency' => 'USD',
                ]
        ];
    }

    /**
     * Incorrect transaction
     * @return array
     */
    protected function getTestTransactionsBroken()
    {
        return [
            0 =>
                [
                    'transaction_id' => '3d2868b6-6749-11e9-8d49-5404a696003a',
                    'type' => 1,
                    'balance_type' => 'card',
                    'balance_id' => 12,
                    'status' => 1,
                    'user_id' => 1,
                    'currency' => 'TL',
                ]
        ];
    }

    /**
     * Converting a set of inputs to array of BalanceTransaction entities
     * @return array
     */
    protected function getEntitiesSet()
    {
        $transactions = [];
        foreach ($this->getTestTransactions() as $key => $tr) {

            $balanceTransaction = new BalanceTransaction();
            $balanceTransaction->setTransactionId($tr['transaction_id']);
            $balanceTransaction->setUserId($tr['user_id']);
            $balanceTransaction->setBalanceId($tr['balance_id']);
            $balanceTransaction->setBalanceType($tr['balance_type']);
            $balanceTransaction->setType($tr['type']);
            $balanceTransaction->setStatus(BalanceTransaction::STATUS_NEW);
            $balanceTransaction->setAmount($tr['amount']);
            $balanceTransaction->setCurrency($tr['currency']);
            $balanceTransaction->setCreatedAt(new \DateTime());
            $balanceTransaction->setUpdatedAt(new \DateTime());

            $transactions[] = $balanceTransaction;
        }
        return $transactions;
    }
}