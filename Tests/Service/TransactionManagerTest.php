<?php

namespace Moneyfge\BalanceBundle\Tests\Service;

use Moneyfge\BalanceBundle\Entity\BalanceTransaction;
use Moneyfge\BalanceBundle\Service\TransactionManager;
use Doctrine\Common\Persistence\ObjectRepository;

class TransactionManagerTest extends AbstractServiceTest
{
    const UUID = '10219c72-6727-11e9-a923-1681be663d3e';

    /**
     * Check creation of BalanceTransaction entity.
     * @return void
     * @throws \Exception
     */
    public function testNewTransactionSuccess()
    {
        $transactionManager = $this->getManager();
        $transaction = $transactionManager->createTransaction(self::UUID, [
            'type' => BalanceTransaction::TYPE_DEPOSIT,
            'user_id' => 1,
            'balance_id' => 11,
            'balance_type' => 'card',
            'currency' => 'USD',
            'amount' => 100.43
        ]);
        $this->assertInstanceOf(BalanceTransaction::class, $transaction);
    }

    /**
     * Adding mock repository to mock entity manager from inherited method.
     * @param bool $noEntity
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getMockEntityManager($noEntity = false)
    {
        $mockEntityManager = parent::getMockEntityManager();

        $balanceTransactionRepository = $this->createMock(ObjectRepository::class);

        if (!$noEntity) {
            $balanceTransactionRepository->expects($this->any())
                ->method('findOneBy')
                ->willReturn($this->getBalanceTransactionEntity());
        }

        $mockEntityManager->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($balanceTransactionRepository));

        return $mockEntityManager;
    }

    /**
     * Creating TransactionManager instance.
     * @param bool $noEntity
     * @return TransactionManager
     */
    protected function getManager($noEntity = false)
    {
        $em = $noEntity
            ? $this->getMockEntityManager(true)
            : $this->getMockEntityManager();

        return new TransactionManager($em);
    }

    /**
     * Return fake BalanceTransaction entity for mock repository
     * @return BalanceTransaction
     */
    protected function getBalanceTransactionEntity()
    {
        $balanceTransaction = new BalanceTransaction();
        $balanceTransaction->setTransactionId(self::UUID);
        $balanceTransaction->setUserId(1);
        $balanceTransaction->setBalanceId(11);
        $balanceTransaction->setBalanceType('card');
        $balanceTransaction->setType(BalanceTransaction::TYPE_DEPOSIT);
        $balanceTransaction->setStatus(BalanceTransaction::STATUS_NEW);
        $balanceTransaction->setAmount(100);
        $balanceTransaction->setCurrency('USD');
        $balanceTransaction->setCreatedAt(new \DateTime());
        $balanceTransaction->setUpdatedAt(new \DateTime());

        return $balanceTransaction;
    }

}