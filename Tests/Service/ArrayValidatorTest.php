<?php

namespace Moneyfge\BalanceBundle\Tests\Service;

use Moneyfge\BalanceBundle\ArrayType\TransactionType;
use Moneyfge\BalanceBundle\Service\ArrayValidator;

class ArrayValidatorTest extends AbstractServiceTest
{

    /**
     * Checks correct data set.
     * @return void
     */
    public function testArrayValidatorPass()
    {
        $arrayValidator = new ArrayValidator();
        $errors = $arrayValidator->validateRequest(new TransactionType(), $this->getDataSets('success'));

        $this->assertEquals(0, count($errors));
    }

    /**
     * Checks data set with excessive field.
     * @return void
     */
    public function testArrayValidatorExcessiveField()
    {
        $arrayValidator = new ArrayValidator();
        $errors = $arrayValidator->validateRequest(new TransactionType(), $this->getDataSets('excess'));

        $this->assertEquals(1, count($errors));
    }

    /**
     * Checks data set with missing field.
     * @return void
     */
    public function testArrayValidatorMissingField()
    {
        $arrayValidator = new ArrayValidator();
        $errors = $arrayValidator->validateRequest(new TransactionType(), $this->getDataSets('missing'));

        $this->assertEquals(1, count($errors));
    }

    /**
     * Checks data set with wrong field type.
     * @return void
     */
    public function testArrayValidatorWrongType()
    {
        $arrayValidator = new ArrayValidator();
        $errors = $arrayValidator->validateRequest(new TransactionType(), $this->getDataSets('wrongType'));

        $this->assertEquals(1, count($errors));
    }

    /**
     * Sets of data.
     * @param string $case
     * @return array
     */
    protected function getDataSets($case = 'success')
    {
        $cases = [
            'success' => [
                'type' => rand(1, 2),
                'user_id' => rand(1, 1000),
                'balance_type_from' => 'typeFrom',
                'balance_type_to' => 'typeTo',
                'system_from' => 'system1',
                'system_to' => 'system2',
                'balance_id_from' => rand(1, 1000),
                'balance_id_to' => rand(1, 1000),
                'amount' => rand (1*100, 100*100) / 100,
                'currency_from' => 'USD',
                'currency_to' => 'EUR'
            ],
            'excess' => [
                'type' => rand(1, 2),
                'user_id' => rand(1, 1000),
                'balance_type_from' => 'typeFrom',
                'balance_type_to' => 'typeTo',
                'system_from' => 'system1',
                'system_to' => 'system2',
                'balance_id_from' => rand(1, 1000),
                'balance_id_to' => rand(1, 1000),
                'amount' => rand (1*100, 100*100) / 100,
                'currency_from' => 'USD',
                'currency_to' => 'EUR',
                'excess' => 'field'
            ],
            'missing' => [
                'type' => rand(1, 2),
                'user_id' => rand(1, 1000),
                'balance_type_from' => 'typeFrom',
                'balance_type_to' => 'typeTo',
                'system_from' => 'system1',
                'system_to' => 'system2',
                'balance_id_to' => rand(1, 1000),
                'amount' => rand (1*100, 100*100) / 100,
                'currency_from' => 'USD',
                'currency_to' => 'EUR',
            ],
            'wrongType' => [
                'type' => rand(1, 2),
                'user_id' => rand(1, 1000),
                'balance_type_from' => 'typeFrom',
                'balance_type_to' => 'typeTo',
                'system_from' => 'system1',
                'system_to' => 'system2',
                'balance_id_from' => rand(1, 1000),
                'balance_id_to' => rand(1, 1000),
                'amount' => 'oneHundredDollars',
                'currency_from' => 'USD',
                'currency_to' => 'EUR'
            ],
        ];

        return $cases[ $case ] ?: $cases['success'];
    }

}