<?php

namespace Moneyfge\BalanceBundle\Tests\Service;

use PHPUnit\Framework\TestCase;

abstract class AbstractServiceTest extends TestCase
{
    /**
     * Build mock entity manager for testing database actions
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getMockEntityManager()
    {
        $entityManagerMock = $this
            ->getMockBuilder('Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()
            ->getMock();
        $entityManagerMock->expects($this->any())
            ->method('persist')
            ->will($this->returnValue(null));
        $entityManagerMock->expects($this->any())
            ->method('flush')
            ->will($this->returnValue(null));

        return $entityManagerMock;
    }
}