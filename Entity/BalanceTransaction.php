<?php

namespace Moneyfge\BalanceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Moneyfge\BalanceBundle\Helper\StringTransformerHelper;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BalanceTransaction
 *
 * @ORM\Table(name="balance_transactions", indexes={@ORM\Index(name="idx", columns={"transaction_id","status"})})
 * @ORM\Entity(repositoryClass="Moneyfge\BalanceBundle\Repository\BalanceTransactionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class BalanceTransaction
{

    const TYPE_DEPOSIT = 1;
    const TYPE_WITHDRAW = 2;

    const STATUS_NEW = 1;
    const STATUS_HOLD = 2;
    const STATUS_COMPLETED = 3;
    const STATUS_CANCELLED = 4;

    const ACTIVE_TYPES = [
        self::STATUS_NEW,
        self::STATUS_HOLD
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     * @Assert\NotBlank
     * @Assert\Type("integer")
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="balance_id", type="integer")
     * @Assert\NotBlank
     * @Assert\Type("integer")
     */
    private $balanceId;

    /**
     * @var string
     *
     * @ORM\Column(name="balance_type", type="string", length=255)
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @Assert\Length(max=255)
     */
    private $balanceType;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=8)
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @Assert\Length(max=8)
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(type="decimal", precision=20, scale=8)
     * @Assert\NotBlank
     * @Assert\Type("numeric")
     * @Assert\GreaterThan(0)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction_id", type="string", length=36, unique=true)
     * @Assert\NotBlank
     * @Assert\Type("string")
     * @Assert\Length(max=36)
     */
    private $transactionId;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint")
     * @Assert\NotBlank
     * @Assert\Type("integer")
     * @Assert\Choice({ 1, 2 })
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     * @Assert\NotBlank
     * @Assert\Type("integer")
     * @Assert\Choice({ 1, 2, 3, 4 })
     */
    private $status = self::STATUS_NEW;

    /**
     * @var \DateTime $createdAt
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime $updatedAt
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return BalanceTransaction
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set balanceId
     *
     * @param integer $balanceId
     *
     * @return BalanceTransaction
     */
    public function setBalanceId($balanceId)
    {
        $this->balanceId = $balanceId;

        return $this;
    }

    /**
     * Get balanceId
     *
     * @return int
     */
    public function getBalanceId()
    {
        return $this->balanceId;
    }

    /**
     * Set balanceType
     *
     * @param string $balanceType
     *
     * @return BalanceTransaction
     */
    public function setBalanceType($balanceType)
    {
        $this->balanceType = $balanceType;

        return $this;
    }

    /**
     * Get balanceType
     *
     * @return string
     */
    public function getBalanceType()
    {
        return $this->balanceType;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return BalanceTransaction
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return BalanceTransaction
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set transactionId
     *
     * @param string $transactionId
     *
     * @return BalanceTransaction
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;

        return $this;
    }

    /**
     * Get transactionId
     *
     * @return string
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return BalanceTransaction
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return BalanceTransaction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return $this
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist()
     */
    public function onSetCreatedAt()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
    }

    /**
     * @ORM\PreUpdate()
     */
    public function onSetUpdatedAt()
    {
        $this->setUpdatedAt(new \DateTime());
    }

}

