<?php

namespace Moneyfge\BalanceBundle\Entity;

use Moneyfge\BalanceBundle\BundleInterface\BalanceInterface;

class RawBalance implements BalanceInterface
{
    public function getId()
    {
        return 1;
    }

    public function plusBalance($amount)
    {
        return $this;
    }

    public function minusBalance($amount)
    {
        return $this;
    }

    public function hasEnoughFunds($amount)
    {
        return true;
    }
}