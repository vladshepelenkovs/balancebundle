# BalanceBundle

### 1. Installation

At first, you should set up your SSH key

    $ ssh-keygen
    $ eval `ssh-agent`
    $ ssh-add ~/.ssh/<private_key_file> (default - id_rsa)
    $ cat ~/.ssh/<public_key_file> (default - id_rsa.pub) and copy it
    add the public key to your Bitbucket settings (choose Bitbucket settings from your avatar in the lower left)
    
Then, add the next records to composer.json in your project

    "require": {
        ...
        "moneyfge/balance-bundle" : "dev-master"
    },
    "repositories" : [{
        "type" : "vcs",
        "url" : "git@bitbucket.org:moneyfgee/balancebundle.git"
    }],
    
and run
    
    composer update moneyfge/balance-bundle

Now, register your bundle in symfony Kernel:

    new Moneyfge\BalanceBundle\BalanceBundle(),
    
or

    Moneyfge\BalanceBundle\BalanceBundle::class => ['all' => true],
    
depending on your symfony version and how you have configured your application.

Bundle needs the next bunch of parameters required in your main configuration file.

    balance:
        bundle_host: "%bundle_host%" # Current application url
        current_system: "%current_system%" # Name of system on which bundle installed
        current_system_display_name: "%current_system_display_name%"
        balance_service_host: "%balance_service_host%" # Url address of balance microservice
        active_transactions_limit: "%active_transactions_limit%" Number of allowed active transactions for user
        allowedBalancesTypes: "%allowed_balances%"
        # Routes, provided below, have default values and maybe it will be better don't touch them without reason
        routes: # Routes, needed for sending/getting data from microservice
            get_balances: "%balances_route%"
            get_rates: "%rates_route%"
            create_transaction: "%transaction_route%"
        serialization_groups:
            balances: ['Balance']

`!Note:` Bundle suggests, that current system has "moneyfge/sso-auth-bundle" installed with server2server auth provided.

Now, you should run 

    php {app/bin}/console doctrine:migrations:diff
    
to create migration with balance_transactions table. Make sure that migration was correctly created and apply this migration.

For proper work of some parts of bundle, you need to be sure that you have include next settings in your config

    framework:
        ...
        validation:      { enable_annotations: true }
        serializer:      { enable_annotations: true }
        
Provide routing to BalanceBundle ServiceController, to enable API points for systems cooperation.

    balance:
        resource: "@BalanceBundle/Resources/config/routing.yml"
        prefix:   /prefix

After bundle was loaded, enabled and configured, you should create new service anywhere in your project and extend it with abstract BalanceService class, provided by bundle, like on example below. Some of it's methods are already implemented and ready for use, but implementation of others is different for specific system, so these methods should be implemented by developer on system. Full signature of Moneyfge\BalanceBundle\Service\AbstractBalanceService class will be provided below.

    class DefaultService extends Moneyfge\BalanceBundle\Service\BalanceService
    {
        public function __construct(ParameterBagInterface $params, ArrayValidator $arrayValidator,
                                HttpTransport $httpTransport, TransactionWriter $transactionWriter,
                                EntityManagerInterface $entityManager, ...your params)
        {
            parent::__construct($params, $arrayValidator, $httpTransport, $transactionWriter, $entityManager);
            //your params
        }
    
        // Here will be implementation of abstract bundle methods
    }

`!Note` Don't forget to call parent::__construct(); method with it's parameters.

And next step you should register your service in application service container.

    balance.balance_service:
      class: CustomBundle\Service\DefaultService
      parent: balance.balance_service_parent
      arguments: [ your arguments ]
      
`!Note` You should name service exactly 'balance.balance_service' to override it's default behaviour, provided by BalanceBundle.

Now, bundle is installed and ready for work.

### 2. BundleService signature

As was mentioned above, in your system you should use your own service which extends Moneyfge\BalanceBundle\Service\AbstractBalanceService service. Some of it's methods are already implemeted and ready for use and some of them you should implement in your service you can see the diagram of class below.

![alt balance_service_signature](docs/bs_signature.png)

##### Implemented methods:
------
**getBalances(data: array, direction: integer): array**  
This method provides an array of user balances from other systems, depending on transfer direction (some directions can be forbidden by balance microservice). This method requires the next set of data.
    
    Directions mathes transactions types
    1 - deposit (add money to current system)
    2 - withdraw (remove money from current system) 
    
    Example input data set
    [
        'user_id' => 1, <-- required
        'balance_type' => 'card',
        'currency' => [ 'USD', 'EUR' ]
    ]
    
    Example output
    [
        [
            'system' => 'worldfge',
            'display_name' => 'WorldFGE Wallet',
            'id' => 32,
            'user_id' => 1,
            'balance' => 10000,
            'currency' => 'KWD',
            'type' => 'wallet'
        ]
    ]
    
------
**getRates(data: array): array**  
Provides an array of currency exchange rates from microservice. Returns all available rates if no parameters sent.
    
    Example input data set
    [
        'currency_from' => 'USD',
        'currency_to' => 'EUR'
    ]
    
    Example response
    [
        [
            'rate' => 2.94749575,
            'base_currency' => 'KWD',
            'quoted_currency' => 'EUR'
        ],
        [
            'rate' => 0.15311825,
            'base_currency' => 'TL',
            'quoted_currency' => 'EUR'
        ]
    ]
    
------
**createTransaction(data: array): mixed**  
Sends request to balance microservice to create transaction and start money transfer process. Also this method creates transaction record in balance_transactions table. Note, that user cannot create more active transactions, than value, provided in config.
    
    Example input data set
    [
        'type' => 1, <-- required
        'user_id' => 1, <-- required
        'balance_id_from' => 21, <-- required
        'balance_id_to' => 11, <-- required
        'currency_from' => 'EUR', <-- required
        'currency_to' => 'USD', <-- required
        'system_to' => 'moneyfge', <-- required
        'system_from' => 'bitinterpay', <-- required
        'amount' => 100.22, <-- required
        'balance_type_from' => 'wallet', <-- required
        'balance_type_to' => 'card' <-- required
    ]
    
    Example response
    [
        'transaction_id' => '0a711b86-6596-11e9-9e25-5404a696003a',
        'rate' => 1.13308070
    ]


##### Abstract methods:
------
**getBalancesFromService(userId: integer, balanceType = null: string, currency = []: array): array**  
This method returns users balances to be collected on microservice and returned to system which made getBalances request.
`! Note` Methods should return array of entities, which are using 'Balances' serialization group (described in config) to be converted to array structure, provided below.
    
    Serialization result example:
    [
        'system' => 'moneyfge',
        'display_name' => 'MoneyFGE wallet',
        'id' => 12,
        'user_id' => 1,
        'balance' => 10000.0,
        'currency' => 'TL',
        'type' => 'card'
    ]

------
**blockBalance(userId: integer, currency: string, balanceId: integer, balanceType: string): string|null**  
Blocks users balance in the way, common for specific system and returns string blockKey, if balance was blocked, or null result, if is not.
    
------
**releaseBalance(blockKey: string, balanceType: string): bool**  
Releases blocked balance by string blockKey, received from blockBalance() method and balanceType.
    
------
**getBalancesCollection(balances: array): array**  
To handle money transfer and provide transaction process, BalanceService need to work with entity, which describes balance in specific system. To be sure, that provided entity has needed methods, developer should force this entity to implement Moneyfge\BalanceBundle\BundleInterface\BalanceInterface, which signature will be described in the next section. 
Consumes array of next structure:

    [
        ['id' => 1, 'balance_type' => 'card'],
        ...
    ]

------
**getBalanceType(balance: BalanceInteface)**  
Consumes balance entity which implements BalanceInterface class and returns it's type.  
`! Note` allowed balance types should be configured in config.
------
**notifyError(message: string): void**  
This method called, if something went wrong on updating transactions during transfer process. Expected, that there will be an email sent, but you are free to use any other way to inform about error.

------
**onHold(balance: BalanceInterface, amount: float): void**  
Action, called after money was removed from balance. You are free to do anything here.

------
**onCompleted(balance: BalanceInterface, amount: float): void**  
Action, called after money was added to balance. You are free to do anything here.

------
**onRefund(balance: BalanceInterface, amount: float): void**  
Action, called after refund was made. You are free to do anything here.

### 3. BalanceInterface signature

Balance entity, provided by system, should implement Moneyfge\BalanceBundle\BundleInterface\BalanceInterface, to make BalanceService sure, that it has needed methods. Interface has the next signature.

![alt balalce_interface_diagram](docs/bi_diagram.png)

##### Methods description:
------
**getId(): integer**  
Returns balance ID.
------
**plusBalance(amount: float): BalanceInterface**  
Extra setter, using to add amount of money to balance. As usual setter, returns entity.
------
**minusBalance(amount: float): BalanceInterface**  
Extra setter, using to remove amount of money from balance. As usual setter, returns entity.
------
**hasEnoughFunds(amount: float): bool**  
Checks, if provided balance has enogh funds to make withdraw operation. Return boolean value.
 
### 4. API points

To provide server2server cooperation, BalanceBundle provides 2 routes to handle requests, coming from Balance Microservice.

**GET: /balances**  

Returns balances from system. 

    Status codes:
        200 - success response
        400 - error occurred
    
    Input example:
    {
        "user_id": 1, <-- required
        "balance_type": "card",
        "currency": [ "USD", "EUR" ]
    }
    
    Output example:
    {
        "status": "success",
        "timestamp": 1556007698,
        "data": {
            "wallets": [
                {
                    "system": "moneyfge",
                    "display_name": "MoneyFGE wallet",
                    "id": 12,
                    "user_id": 1,
                    "balance": 10000,
                    "currency": "TL",
                    "type": "card"
                }
            ]
        }
    }
    
**POST: /transactions**  

Handles set of transactions and returns result of made operations. 

    Status codes:
        200 - success response
        400 - an error occurred 
    
    Input example:
    {
        "0": {
            "transaction_id": "8687fa8d-65a2-11e9-9e25-5404a696003a",
            "type": 2,
            "balance_type": "card",
            "balance_id": 12,
            "amount": 12.425545987500001,
            "status": 1,
            "user_id": 1,
            "currency": "TL"
        },
        "5": {
            "transaction_id": "86894b23-65a2-11e9-9e25-5404a696003a",
            "type": 2,
            "balance_type": "wallet",
            "balance_id": 13,
            "amount": 262.8424156725,
            "status": 1,
            "user_id": 1,
            "currency": "KWD"
        }
    }
    
    Output example:
    {
        "status": "success",
        "timestamp": 1556008600,
        "data": [
            {
                "result": 1,
                "message": "Success",
                "transaction_id": "8687fa8d-65a2-11e9-9e25-5404a696003a"
            },
            {
                "result": 1,
                "message": "Success",
                "transaction_id": "868aa6bc-65a2-11e9-9e25-5404a696003a"
            }
        ]
    }
    
### 5. Database structure
BalanceBundle requires balance_transactions table to work. As described in installation instructions, you should run next command to create migration.

    php app/console doctrine:migrations:diff
    
or

    php bin/console make:migration
    
which depends on which version of symfony used in system.
After it, make sure that migration was correctly generated. balance_transactions table should have the next structure:

    CREATE TABLE balance_transactions (
        id BIGINT AUTO_INCREMENT NOT NULL, 
        user_id INT NOT NULL, 
        balance_id INT NOT NULL, 
        balance_type VARCHAR(255) NOT NULL, 
        currency VARCHAR(8) NOT NULL, 
        amount NUMERIC(20, 8) NOT NULL, 
        transaction_id VARCHAR(36) NOT NULL, 
        type SMALLINT NOT NULL, 
        status SMALLINT NOT NULL, 
        created_at DATETIME NOT NULL, 
        updated_at DATETIME NOT NULL, 
        UNIQUE INDEX UNIQ_A479A1FA2FC0CB0F (transaction_id), 
        INDEX idx (transaction_id, status), 
        PRIMARY KEY(id)
    ) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB
    
Than, run command 

    php {app,bin}/console doctrine:migrations:migrate
    
Some of fields need some extra explanation.

**status**

    Describes current transaction status
    1 - new transaction
    2 - hold transaction (money was removed from system, but were not added to another)
    3 - completed (money was added to needed balance)
    4 - cancelled (transaction faced and error/problem on some step and was cancelled)
    
    1, 2 - are active statuses
    3, 4 - finished transaction, it means that there will be no more actions with user's balance
    
**type**

    Tells, what bundle should do with money - add or remove
    1 - deposit type
    2 - withdraw type




