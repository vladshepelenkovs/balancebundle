<?php

namespace Moneyfge\BalanceBundle\BundleInterface;

use Symfony\Component\Validator\Constraints as Assert;

interface ArrayTypeInterface
{

    /**
     * @return Assert\Collection mixed
     */
    public function getValidationRules();

}