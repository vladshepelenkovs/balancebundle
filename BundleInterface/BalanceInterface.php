<?php

namespace Moneyfge\BalanceBundle\BundleInterface;

/**
 * Interface BalanceInterface
 * @package Moneyfge\BalanceBundle\BundleInterface
 *
 * This interface need to be applied to balance entity in installed system
 * to make BalanceService sure, that we works with correct entity, which
 * has needed methods.
 */
interface BalanceInterface
{
    /**
     * Return balance ID.
     * @return integer
     */
    public function getId();

    /**
     * Add funds setter for system balance entity.
     * As common entity setter, returns entity.
     *
     * @param double $amount
     * @return BalanceInterface
     */
    public function plusBalance($amount);

    /**
     * Remove funds setter for system balance entity.
     * As common entity setter, returns entity.
     *
     * @param double $amount
     * @return BalanceInterface
     */
    public function minusBalance($amount);

    /**
     * Checks, if balance has enough money to handle transaction.
     *
     * @param double $amount
     * @return bool
     */
    public function hasEnoughFunds($amount);

}