<?php

namespace Moneyfge\BalanceBundle\Helper;

use Symfony\Component\Validator\ConstraintViolationListInterface;

trait ConstraintHelper
{

    /**
     * Conversion from violations list to assoc array
     *
     * @param ConstraintViolationListInterface $violations
     * @return array
     */
    protected function constraintViolationListToArray(ConstraintViolationListInterface $violations)
    {
        $resultArray = [];

        foreach ($violations as $violation) {
            $resultArray[$violation->getPropertyPath()] = $violation->getMessage();
        }

        return $resultArray;
    }

    /**
     * Conversion from violations list to string
     *
     * @param ConstraintViolationListInterface $violations
     * @return string
     */
    protected function constraintViolationListToString(ConstraintViolationListInterface $violations)
    {
        $resultString = 'Wrong input data: ';

        foreach ($violations as $violation) {
            $resultString .= $violation->getPropertyPath() . " - " . $violation->getMessage() . " ";
        }

        return $resultString;
    }

}