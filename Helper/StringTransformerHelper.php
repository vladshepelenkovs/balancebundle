<?php

namespace Moneyfge\BalanceBundle\Helper;

trait StringTransformerHelper
{

    /**
     * Transforms an under_scored_string to a camelCasedOne
     *
     * @param string $scored
     * @return string
     */
    function camelize($scored) {
        return lcfirst(
            implode(
                '',
                array_map(
                    'ucfirst',
                    array_map(
                        'strtolower',
                        explode(
                            '_', $scored)))));
    }

    /**
     * Transforms a camelCasedString to an under_scored_one
     *
     * @param string $cameled
     * @return string
     */
    function underscore($cameled) {
        return implode(
            '_',
            array_map(
                'strtolower',
                preg_split('/([A-Z]{1}[^A-Z]*)/', $cameled, -1, PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY)));
    }

}