<?php

namespace Moneyfge\BalanceBundle\Helper;

use Doctrine\Common\Collections\ArrayCollection;

trait EntityCollectionMap
{
    /**
     * Map provided collection of entities with provided getter name.
     * @param ArrayCollection $collection
     * @param string $getter
     * @return ArrayCollection
     */
    protected function mapCollectionByIndex(ArrayCollection $collection, $getter)
    {
        $result = new ArrayCollection();

        foreach ($collection as $element) {
            $result->set( $element->$getter(), $element );
        }

        return $result;
    }

}