<?php

namespace Moneyfge\BalanceBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Moneyfge\BalanceBundle\BundleInterface\BalanceInterface;
use Moneyfge\BalanceBundle\Entity\RawBalance;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class RawService extends AbstractBalanceService
{
    public function __construct(ParameterBagInterface $params, ArrayValidator $arrayValidator,
                                HttpService $httpService, TransactionManager $transactionManager,
                                EntityManagerInterface $entityManager)
    {
        parent::__construct($params, $arrayValidator, $httpService, $transactionManager, $entityManager);
    }

    public function getBalancesFromService($userId, $balanceType = null, $currency = [])
    {
        return [];
    }

    public function blockBalance($userId, $currency, $balanceId, $balanceType)
    {
        return 'stringValue';
    }

    public function releaseBalance($blockKey, $balanceType)
    {
        return true;
    }

    public function getBalancesCollection($balances)
    {
        return [];
    }

    public function getBalanceType(BalanceInterface $balance)
    {
        return 'string';
    }

    public function notifyError($message)
    {

    }

    public function onHold(BalanceInterface $balance, $amount)
    {

    }

    public function onCompleted(BalanceInterface $balance, $amount)
    {

    }

    public function onRefund(BalanceInterface $balance, $amount)
    {

    }
}