<?php

namespace Moneyfge\BalanceBundle\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class HttpTransport
{

    const QUERY_METHODS = ['GET','HEAD'];
    const BODY_METHODS = ['POST','PUT','DELETE'];

    protected $params;

    protected $client;

    public function __construct(ParameterBagInterface $params, Client $client)
    {
        $this->params = $params;
        $this->client = $client;
    }

    /**
     * Getting request url
     *
     * @param string $routeName
     * @return string
     */
    public function getUrl($routeName)
    {
        return $this->params->get('balance.balance_service_host')
            . $this->params->get('balance.routes')[$routeName];
    }

    /**
     * Composing response array for output
     *
     * @param array|bool $response
     * @param string $url
     * @return array
     */
    public function composeResponse($response, $url = null)
    {
        $errors = [];
        $requestSuccess = isset($response['status']) && $response['status'] === 'success';

        if (false === $response || !is_array($response) || !$requestSuccess) {
            $errors[] = [
                'source' => $this->params->get('balance.bundle_host'),
                'error' => 'Cannot get response from ' . $url
            ];
        }

        return [
            'data' => isset($response['data']) ? $response['data'] : [],
            'errors' => !empty($response['errors'])
                ? $response['errors']
                : $errors
        ];
    }

    /**
     * Compose and sends request, returning response
     *
     * @param string $url
     * @param string $method
     * @param array $headers
     * @param array $params
     * @return array|mixed|\Psr\Http\Message\StreamInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendRequest($url, $method, $headers = [], $params = [])
    {
        $options = [];
        $headers['Authorization'] = $this->params->get('sso_auth.server_secret');
        $options['headers'] = $headers;

        if (in_array(strtoupper($method), self::QUERY_METHODS)) {
            $options['query'] = $params;
        }

        if (in_array(strtoupper($method), self::BODY_METHODS)) {
            $options['json'] = $params;
        }

        try {
            $response = $this->client->request($method, $url, $options)->getBody();
            $response = json_decode($response, true);
        } catch (RequestException $exception) {
            $response = json_decode( (string)$exception->getResponse()->getBody(), true);
        }

        return $response;
    }
}