<?php

namespace Moneyfge\BalanceBundle\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class HttpService
{

    /**
     * @var ParameterBagInterface
     */
    private $params;

    /**
     * @var HttpTransport
     */
    private $httpTransport;

    /**
     * HttpTransport constructor.
     * @param ParameterBagInterface $params
     * @param HttpTransport $httpTransport
     */
    public function __construct(ParameterBagInterface $params, HttpTransport $httpTransport)
    {
        $this->params = $params;
        $this->httpTransport = $httpTransport;
    }

    /**
     * Getting user accounts from other services.
     * @param array $body
     * @param integer $type
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getBalances($body, $type)
    {
        $body['system_name'] = $this->params->get('balance.current_system');

        $url = $this->httpTransport->getUrl('get_balances');
        $url .= "/$type";

        $response = $this->httpTransport->sendRequest($url, 'GET',[], $body);

        return $this->httpTransport->composeResponse($response, $url);
    }

    /**
     * Getting rates from Balance Service.
     * @param array $body
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getRates($body)
    {
        $url = $this->httpTransport->getUrl('get_rates');
        $response = $this->httpTransport->sendRequest($url, 'GET', [], $body);

        return $this->httpTransport->composeResponse($response, $url);
    }

    /**
     * Request for creating money transaction on balance microservice.
     * @param array $body
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createTransaction($body)
    {
        $url = $this->httpTransport->getUrl('create_transaction');
        $response = $this->httpTransport->sendRequest($url, 'POST' , [], $body);

        return $this->httpTransport->composeResponse($response, $url);
    }

}