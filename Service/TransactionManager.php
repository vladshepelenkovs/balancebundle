<?php

namespace Moneyfge\BalanceBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Moneyfge\BalanceBundle\Entity\BalanceTransaction;

class TransactionManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * TransactionWriter constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Creates transaction record in database.
     * @param string $uuid
     * @param array $data
     * @return BalanceTransaction
     * @throws \Exception
     */
    public function createTransaction($uuid, $data)
    {
        $transaction = new BalanceTransaction();

        $transaction->setTransactionId($uuid);
        $transaction->setStatus(BalanceTransaction::STATUS_NEW);

        if (!isset($data['balance_id'])) {

            switch ($data['type']) {
                case BalanceTransaction::TYPE_DEPOSIT:
                    $data['balance_id'] = $data['balance_id_to'];
                    $data['balance_type'] = $data['balance_type_to'];
                    $data['currency'] = $data['currency_to'];
                    break;
                case BalanceTransaction::TYPE_WITHDRAW:
                    $data['balance_id'] = $data['balance_id_from'];
                    $data['balance_type'] = $data['balance_type_from'];
                    $data['currency'] = $data['currency_from'];
                    break;
                default:
                    throw new \Exception('Unknown transaction type');
                    break;
            }

        }

        $transaction->setType($data['type']);
        $transaction->setBalanceType($data['balance_type']);
        $transaction->setBalanceId($data['balance_id']);
        $transaction->setAmount($data['amount']);
        $transaction->setUserId($data['user_id']);
        $transaction->setCurrency($data['currency']);

        $this->entityManager->persist($transaction);
        return $transaction;
    }

    /**
     * Calls flush for classes, which are using this service, but don't
     * have injected entity manager.
     * @return void
     */
    public function commit()
    {
        $this->entityManager->flush();
    }
}