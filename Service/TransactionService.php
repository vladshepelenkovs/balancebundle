<?php

namespace Moneyfge\BalanceBundle\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Moneyfge\BalanceBundle\ArrayType\TransactionsType;
use Moneyfge\BalanceBundle\BundleInterface\BalanceInterface;
use Moneyfge\BalanceBundle\Entity\BalanceTransaction;
use Moneyfge\BalanceBundle\Helper\EntityCollectionMap;

class TransactionService
{
    use EntityCollectionMap;

    const ACTION_HOLD_MONEY = 1;
    const ACTION_ADD_MONEY = 2;
    const ACTION_FINISH_TRANSACTION = 3;
    const ACTION_MARK_EXPIRED = 4;
    const ACTION_MARK_ERROR_RECEIVER = 5;
    const ACTION_MARK_ERROR_SENDER = 6;
    const ACTION_MARK_REFUND = 7;
    const ACTION_MAKE_REFUND = 8;

    const RESULT_TYPE_SUCCESS = 1;
    const RESULT_TYPE_NOT_ENOUGH_FUNDS = 2;
    const RESULT_TYPE_WITHDRAW_ERROR = 3;
    const RESULT_TYPE_DEPOSIT_ERROR = 4;
    const RESULT_TYPE_NO_BALANCE = 5;
    const RESULT_TYPE_INCORRECT_TRANSACTION = 6;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var AbstractBalanceService
     */
    protected $balanceService;

    /**
     * @var TransactionManager
     */
    protected $transactionManager;

    /**
     * @var ArrayValidator
     */
    protected $arrayValidator;

    /**
     * All balances, blocked for performing operations with them during transaction process.
     * @var array
     */
    protected $blockedBalances;

    /**
     * TransactionManager constructor.
     * @param EntityManagerInterface $entityManager
     * @param AbstractBalanceService $balanceService
     * @param TransactionManager $transactionManager
     * @param ArrayValidator $arrayValidator
     */
    public function __construct(EntityManagerInterface $entityManager, AbstractBalanceService $balanceService,
                                TransactionManager $transactionManager, ArrayValidator $arrayValidator)
    {
        $this->entityManager = $entityManager;
        $this->balanceService = $balanceService;
        $this->transactionManager = $transactionManager;
        $this->arrayValidator = $arrayValidator;
    }

    /**
     * Handling transactions list and making correct interactions with each transaction.
     * @param array $data
     * @return array|bool
     * @throws \Exception
     */
    public function handleTransactions($data)
    {
        $errors = $this->arrayValidator->validateRequest(new TransactionsType(), $data);

        if (0 !== count($errors)) {
            throw new \Exception('Wrong input data!');
        }

        $result = [];
        $transactionsIds = $this->getTransactionIds($data);
        $balanceTransactionsRepository = $this->entityManager->getRepository(BalanceTransaction::class);

        $transactions = new ArrayCollection( $balanceTransactionsRepository->findBy(['transactionId' => $transactionsIds]) );
        $transactions = $this->mapCollectionByIndex($transactions, 'getTransactionId');
        $transactions = $this->createNonExistingTransactions($transactions, $data);

        $this->blockBalances($transactions);
        $blockKeys = $this->blockedBalances;

        $transactions = $transactions->filter(function (BalanceTransaction $element) use ($blockKeys) {
            return array_key_exists($element->getTransactionId(), $blockKeys);
        });

        $balancesCollection = new ArrayCollection(
            $this->balanceService->getBalancesCollection( $this->formGetBalancesCollectionData($transactions) ) );

        foreach ($data as $requestedTransaction) {

            $transaction = $transactions->get($requestedTransaction['transaction_id']);

            $balance = $balancesCollection->filter(function (BalanceInterface $element) use ($requestedTransaction) {
                return $element->getId() == $requestedTransaction['balance_id']
                    && $requestedTransaction['balance_type'] == $this->balanceService->getBalanceType($element);
            })->first();

            if ($transaction && $balance) {
                $result[] = $this->dispatch($requestedTransaction['status'], $transaction, $balance);
            }
        }

        try {
            $this->entityManager->flush();
        } catch (\Exception $e) {
            $this->balanceService->notifyError($e->getMessage());
            return false;
        }

        $this->releaseBalances();
        $this->afterAction($data, $balancesCollection);

        return $result;
    }

    /**
     * Creates transaction record in database if it's missing.
     * @param ArrayCollection $transactions
     * @param array $data
     * @return ArrayCollection
     * @throws \Exception
     */
    protected function createNonExistingTransactions($transactions, $data)
    {
        foreach ($data as $inputTransaction) {
            if (!$transactions->get($inputTransaction['transaction_id'])) {
                $transaction = $this->transactionManager
                    ->createTransaction($inputTransaction['transaction_id'], $inputTransaction);
                $transactions->set($inputTransaction['transaction_id'], $transaction);
            }
        }

        return $transactions;
    }

    /**
     * Getting transaction ids from transaction data array.
     * @param array $transactions
     * @return array
     */
    protected function getTransactionIds($transactions)
    {
        $ids = [];

        foreach ($transactions as $transaction) {
            $ids[] = $transaction['transaction_id'];
        }

        return $ids;
    }

    /**
     * Blocks balances by provided transaction collection.
     * @param ArrayCollection $transactions
     * @return void
     */
    protected function blockBalances(ArrayCollection $transactions)
    {
        $keys = [];

        foreach ($transactions as $transaction) {

            $blockKey = $this->balanceService->blockBalance($transaction->getUserId(),
                $transaction->getCurrency(), $transaction->getBalanceId(), $transaction->getBalanceType());

            $keys[$transaction->getTransactionId()] = [
                'blockKey' => $blockKey,
                'balanceType' => $transaction->getBalanceType()
            ];
        }

        $this->blockedBalances = $keys;
    }

    /**
     * Release all blocked balances, stored in class attribute.
     * @return void
     */
    protected function releaseBalances()
    {
        foreach ($this->blockedBalances as $blockedBalance) {
            $this->balanceService->releaseBalance($blockedBalance['blockKey'], $blockedBalance['balanceType']);
        }
    }

    /**
     * Forms data array for getting balances collection.
     * @param ArrayCollection $transactions
     * @return array
     */
    protected function formGetBalancesCollectionData(ArrayCollection $transactions)
    {
        $result = [];

        foreach ($transactions as $transaction) {
            $result[] = [
                'id' => $transaction->getBalanceId(),
                'balance_type' => $transaction->getBalanceType()
            ];
        }

        return $result;
    }

    /**
     * Dispatching chosen action with transaction.
     * Should return element of resulting array.
     * @param integer $action
     * @param BalanceTransaction $transaction
     * @param BalanceInterface $balance
     * @return array
     * @throws \Exception
     */
    protected function dispatch($action, BalanceTransaction $transaction, BalanceInterface $balance)
    {

        switch ($action) {

            case self::ACTION_HOLD_MONEY:
                $result = $this->removeFunds($transaction, $balance);
                if ($result['result'] === self::RESULT_TYPE_SUCCESS) {
                    $transaction->setStatus(BalanceTransaction::STATUS_HOLD);
                }
                break;

            case self::ACTION_ADD_MONEY:
                $result = $this->addFunds($transaction, $balance);
                if ($result['result'] === self::RESULT_TYPE_SUCCESS) {
                    $transaction->setStatus(BalanceTransaction::STATUS_COMPLETED);
                }
                break;

            case self::ACTION_FINISH_TRANSACTION:
                $transaction->setStatus(BalanceTransaction::STATUS_COMPLETED);
                $result = [
                    'result' => self::RESULT_TYPE_SUCCESS,
                    'message' => 'Success',
                ];
                break;

            case self::ACTION_MARK_EXPIRED:
            case self::ACTION_MARK_ERROR_RECEIVER:
            case self::ACTION_MARK_ERROR_SENDER:
            case self::ACTION_MARK_REFUND:
                $transaction->setStatus(BalanceTransaction::STATUS_CANCELLED);
                $result = [
                    'result' => self::RESULT_TYPE_SUCCESS,
                    'message' => 'Transaction successfully cancelled.',
                ];
                break;

            case self::ACTION_MAKE_REFUND:
                $result = $this->addFunds($transaction, $balance);
                if ($result['result'] === self::RESULT_TYPE_SUCCESS) {
                    $transaction->setStatus(BalanceTransaction::STATUS_CANCELLED);
                    $result = [
                        'result' => self::RESULT_TYPE_SUCCESS,
                        'message' => 'Money was refunded and transaction successfully cancelled.',
                    ];
                }
                break;

            default:
                $result = [
                    'result' => self::RESULT_TYPE_INCORRECT_TRANSACTION,
                    'message' => 'Incorrect transaction data',
                ];
                break;
        }

        $result['transaction_id'] = $transaction->getTransactionId();
        return $result;
    }

    /**
     * Action for money deposit.
     * Balance entity, get from getBalance() methods should implement BalanceInterface.
     * @param BalanceTransaction $transaction
     * @param BalanceInterface $balance
     * @return array
     * @throws \Exception
     */
    protected function addFunds(BalanceTransaction $transaction, BalanceInterface $balance)
    {
        if ($transaction->getStatus() !== BalanceTransaction::STATUS_NEW
            && $transaction->getStatus() !== BalanceTransaction::STATUS_HOLD) {
            return $this->composeActionResponse(self::RESULT_TYPE_SUCCESS, 'Success');
        }

        if (!$balance instanceof BalanceInterface) {
            $this->releaseBalances();
            throw new \Exception('Balance entity should implement BalanceInterface!');
        }

        $balance->plusBalance($transaction->getAmount());

        return $this->composeActionResponse(self::RESULT_TYPE_SUCCESS, 'Success');
    }

    /**
     * Action for withdraw funds from balance.
     * Balance entity, get from getBalance() methods should implement BalanceInterface.
     * @param BalanceTransaction $transaction
     * @param BalanceInterface $balance
     * @return array
     * @throws \Exception
     */
    protected function removeFunds(BalanceTransaction $transaction, BalanceInterface $balance)
    {
        if ($transaction->getStatus() !== BalanceTransaction::STATUS_NEW) {
            return $this->composeActionResponse(self::RESULT_TYPE_SUCCESS, 'Success');
        }

        if (!$balance instanceof BalanceInterface) {
            $this->releaseBalances();
            throw new \Exception('Balance entity should implement BalanceInterface!');
        }

        if (!$balance->hasEnoughFunds($transaction->getAmount())) {
            return $this->composeActionResponse(self::RESULT_TYPE_NOT_ENOUGH_FUNDS, 'Not enough funds on account');
        }

        $balance->minusBalance($transaction->getAmount());

        return $this->composeActionResponse(self::RESULT_TYPE_SUCCESS, 'Success');
    }

    /**
     * Helps to compose response message for add/remove funds methods.
     * @param integer $result
     * @param string string $message
     * @return array
     */
    protected function composeActionResponse($result, $message = '')
    {
        return [
            'result' => $result,
            'message' => $message
        ];
    }

    /**
     * Performing after action events, provided by BalanceService implementation.
     * @param array $data
     * @param ArrayCollection $balancesCollection
     * @return void
     */
    protected function afterAction($data, ArrayCollection $balancesCollection)
    {
        foreach ($data as $transaction) {

            $balance = $balancesCollection->filter(function (BalanceInterface $element) use ($transaction) {
                return $element->getId() == $transaction['balance_id']
                    && $this->balanceService->getBalanceType($element) == $transaction['balance_type'];
            })->first();

            if (!$balance) {
                continue;
            }

            switch ($transaction['status']) {
                case self::ACTION_ADD_MONEY:
                    $this->balanceService->onCompleted($balance, $transaction['amount']);
                    break;
                case self::ACTION_HOLD_MONEY:
                    $this->balanceService->onHold($balance, $transaction['amount']);
                    break;
                case self::ACTION_MAKE_REFUND:
                    $this->balanceService->onRefund($balance, $transaction['amount']);
                    break;
                default:
                    continue;
                    break;
            }
        }
    }

}