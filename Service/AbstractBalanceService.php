<?php

namespace Moneyfge\BalanceBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Moneyfge\BalanceBundle\ArrayType\GetBalancesType;
use Moneyfge\BalanceBundle\ArrayType\RateType;
use Moneyfge\BalanceBundle\ArrayType\TransactionResponseType;
use Moneyfge\BalanceBundle\ArrayType\TransactionType;
use Moneyfge\BalanceBundle\BundleInterface\BalanceInterface;
use Moneyfge\BalanceBundle\Entity\BalanceTransaction;
use Moneyfge\BalanceBundle\Helper\ConstraintHelper;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

abstract class AbstractBalanceService
{
    /**
     * Helper trait for constraint list conversion
     */
    use ConstraintHelper;

    /**
     * @var ParameterBagInterface
     */
    protected $params;

    /**
     * @var ArrayValidator
     */
    protected $arrayValidator;

    /**
     * @var HttpService
     */
    protected $httpService;

    /**
     * @var TransactionManager
     */
    protected $transactionManager;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * Contains last service error
     * @var mixed
     */
    protected $error;

    /**
     * BalanceService constructor.
     * @param ParameterBagInterface $params
     * @param ArrayValidator $arrayValidator
     * @param HttpService $httpService
     * @param TransactionManager $transactionManager
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(ParameterBagInterface $params, ArrayValidator $arrayValidator,
                                HttpService $httpService, TransactionManager $transactionManager,
                                EntityManagerInterface $entityManager)
    {
        $this->params = $params;
        $this->arrayValidator = $arrayValidator;
        $this->httpService = $httpService;
        $this->transactionManager = $transactionManager;
        $this->entityManager = $entityManager;
    }

    /**
     * Requests balances data from other systems
     *
     * @param array $data
     * @param integer $direction
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function getBalances($data, $direction)
    {
        $violations = $this->arrayValidator
            ->validateRequest(new GetBalancesType(), $data);

        if (0 !== count($violations)) {
            throw new \Exception($this->constraintViolationListToString($violations));
        }

        $response = $this->httpService
            ->getBalances($data, $direction);

        if (!empty($response['errors'])) {
            $this->setError($response['errors']);
        }

        return $response['data'];
    }

    /**
     * Loading rate data from balance-service
     *
     * @param array $data
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function getRates($data)
    {
        $violations = $this->arrayValidator
            ->validateRequest(new RateType(), $data);

        if (0 !== count($violations)) {
            throw new \Exception($this->constraintViolationListToString($violations));
        }

        $response = $this->httpService
            ->getRates($data);

        if (!empty($response['errors'])) {
            $this->setError($response['errors']);
        }

        return $response['data'];
    }

    /**
     * Sending request to balance microservice for creating new transaction
     *
     * @param array $data
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function createTransaction($data)
    {
        $activeTransactionsLimit = $this->params->get('balance.active_transactions_limit');
        $balanceTransactionRepository = $this->entityManager->getRepository(BalanceTransaction::class);

        $violations = $this->arrayValidator
            ->validateRequest(new TransactionType(), $data);

        if (0 !== count($violations)) {
            throw new \Exception($this->constraintViolationListToString($violations));
        }

        if (count( $balanceTransactionRepository->findActiveUserTransactions($data['user_id']) ) >= $activeTransactionsLimit) {
            $this->setError('User exceeded max active transactions limit');
            return;
        }

        $response = $this->httpService
            ->createTransaction($data);

        if (!empty($response['errors'])) {
            $this->setError($response['errors']);
            return;
        }

        $violations = $this->arrayValidator
            ->validateRequest(new TransactionResponseType(), $response['data']);

        if (0 !== count($violations)) {
            $this->setError('Wrong transaction response');
            return;
        }

        $data['amount'] = $data['amount'] * $response['data']['rate'];

        $this->transactionManager->createTransaction($response['data']['transaction_id'], $data);

        return $response['data'];
    }

    /**
     * Getting user balances from database by provided params
     *
     * @param integer $userId
     * @param string $balanceType
     * @param array $currency
     *
     * @return array
     */
    abstract public function getBalancesFromService($userId, $balanceType = null, $currency = []);

    /**
     * Blocks balance to perform money transfer.
     * Should return $blockKey on success, otherwise returns false.
     *
     * @param integer $userId
     * @param string $currency
     * @param integer $balanceId
     * @param string $balanceType
     * @return string|null
     */
    abstract public function blockBalance($userId, $currency, $balanceId, $balanceType);

    /**
     * Releases balance after doing some staff.
     * Consumes $blockKey, retrieved from blockBalance() method.
     *
     * @param string $blockKey
     * @param string $balanceType
     * @return bool
     */
    abstract public function releaseBalance($blockKey, $balanceType);

    /**
     * Getting balance entity from system.
     * Returned entity should implement BalanceInterface.
     *
     * @param array $balances
     * @return array
     */
    abstract public function getBalancesCollection($balances);

    /**
     * Returns balance type.
     *
     * @param BalanceInterface $balance
     * @return string
     */
    abstract public function getBalanceType(BalanceInterface $balance);

    /**
     * Sending email with error on failing transactions save/updating.
     *
     * @param string $message
     * @return void
     */
    abstract public function notifyError($message);

    /**
     * Action, performing after holding balance
     *
     * @param BalanceInterface $balance
     * @param $amount
     * @return void
     */
    abstract public function onHold(BalanceInterface $balance, $amount);

    /**
     * Action, performing after adding funds to account
     *
     * @param BalanceInterface $balance
     * @param $amount
     * @return void
     */
    abstract public function onCompleted(BalanceInterface $balance, $amount);

    /**
     * Action, performing after refund action
     *
     * @param BalanceInterface $balance
     * @param $amount
     * @return void
     */
    abstract public function onRefund(BalanceInterface $balance, $amount);

    /**
     * Saving last error.
     *
     * @param mixed $error
     */
    protected function setError($error)
    {
        $this->error = $error;
    }

    /**
     * Get last service error
     *
     * @return mixed
     */
    public function getLastError()
    {
        return $this->error;
    }

}