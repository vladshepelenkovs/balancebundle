<?php

namespace Moneyfge\BalanceBundle\Service;

use Moneyfge\BalanceBundle\BundleInterface\ArrayTypeInterface;
use Symfony\Component\Validator\Validation;

class ArrayValidator
{
    /**
     * @var \Symfony\Component\Validator\ValidatorInterface
     */
    private $validator;

    /**
     * RequestValidator constructor.
     */
    public function __construct()
    {
        $this->validator = Validation::createValidator();
    }

    /**
     * Checking, if input array contains valid request data
     *
     * @param ArrayTypeInterface $requestType
     * @param array $data
     *
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface
     */
    public function validateRequest(ArrayTypeInterface $requestType, $data)
    {
        return $this->validator->validate((array)$data, $requestType->getValidationRules());
    }

}