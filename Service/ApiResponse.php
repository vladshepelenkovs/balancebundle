<?php

namespace Moneyfge\BalanceBundle\Service;

use Moneyfge\BalanceBundle\ArrayType\ErrorResponseType;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

class ApiResponse
{
    /**
     * @var ArrayValidator
     */
    protected $arrayValidator;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * ApiResponse constructor.
     * @param ArrayValidator $arrayValidator
     * @param RequestStack $requestStack
     */
    public function __construct(ArrayValidator $arrayValidator, RequestStack $requestStack)
    {
        $this->arrayValidator = $arrayValidator;
        $this->requestStack = $requestStack;
    }

    /**
     * Return success response.
     * @param integer $code
     * @param array $data
     * @return Response
     */
    public function createSuccessResponse($code, $data = [])
    {
        $response = [
            'status' => 'success',
            'timestamp' => time(),
            'data' => $data
        ];

        return new Response(json_encode($response), $code, [
            'Content-Type' => 'application/json'
        ]);
    }

    /**
     * Return mixed response with both errors and data.
     * @param integer $code
     * @param array $data
     * @param array $errors
     * @return Response
     * @throws \Exception
     */
    public function createMixedResponse($code, $data = [], $errors = [])
    {
        $this->validateErrors($errors);

        $response = [
            'status' => 'success',
            'timestamp' => time(),
            'data' => $data,
            'errors' => $this->composeErrorsArray($errors)
        ];

        return new Response(json_encode($response), $code, [
            'Content-Type' => 'application/json'
        ]);
    }

    /**
     * Creating error response.
     * @param integer $code
     * @param array $errors
     * @return Response
     * @throws \Exception
     */
    public function createErrorResponse($code, $errors = [])
    {
        $this->validateErrors($errors);

        $response = [
            'status' => 'error',
            'timestamp' => time(),
            'errors' => $this->composeErrorsArray($errors)
        ];

        return new Response(json_encode($response), $code, [
            'Content-Type' => 'application/json'
        ]);
    }

    /**
     * Converting array of strings to final response format.
     * @param array $errors
     * @return array
     */
    protected function composeErrorsArray($errors)
    {
        $result = [];
        $source = $this->requestStack->getCurrentRequest()->getUri();

        foreach ($errors as $error) {
            $result[] = [
                'source' => $source ?: '',
                'error' => $error
            ];
        }

        return $result;
    }

    /**
     * Validating errors array.
     * @param array $errors
     * @throws \Exception
     */
    protected function validateErrors($errors)
    {
        $constraints = $this->arrayValidator->validateRequest(new ErrorResponseType(), $errors);

        if (0 !== count($constraints)) {
            throw new \Exception('Wrong error format');
        }
    }

}