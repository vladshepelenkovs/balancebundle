<?php

namespace Moneyfge\BalanceBundle\Service;

use Moneyfge\BalanceBundle\RequestType\RequestTypeInterface;
use Symfony\Component\Validator\Validation;

class RequestValidator
{
    /**
     * @var \Symfony\Component\Validator\ValidatorInterface
     */
    private $validator;

    /**
     * RequestValidator constructor.
     */
    public function __construct()
    {
        $this->validator = Validation::createValidator();
    }

    /**
     * Checking, if input array contains valid request data
     *
     * @param RequestTypeInterface $requestType
     * @param array $data
     *
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface
     */
    public function validateRequest(RequestTypeInterface $requestType, $data)
    {
        return $this->validator->validate($data, $requestType->getValidationRules());
    }

}