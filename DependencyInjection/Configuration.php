<?php

namespace Moneyfge\BalanceBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $treeBuilder->root('balance')
            ->children()
                ->scalarNode('bundle_host')->isRequired()->end()
                ->scalarNode('current_system')->isRequired()->end()
                ->scalarNode('current_system_display_name')->isRequired()->end()
                ->scalarNode('balance_service_host')->isRequired()->end()
                ->integerNode('active_transactions_limit')->isRequired()->end()
                ->arrayNode('allowedBalancesTypes')
                    ->prototype('scalar')->end()
                ->end()
                ->arrayNode('routes')->isRequired()
                    ->children()
                        ->scalarNode('get_balances')->isRequired()->end()
                        ->scalarNode('get_rates')->isRequired()->end()
                        ->scalarNode('create_transaction')->isRequired()->end()
                    ->end()
                ->end()
                ->arrayNode('serialization_groups')->isRequired()
                    ->children()
                        ->arrayNode('balances')->isRequired()->prototype('scalar')->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}