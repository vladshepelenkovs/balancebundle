<?php

namespace Moneyfge\BalanceBundle\Normalizer;

use Moneyfge\BalanceBundle\EntityInterface\BalancesInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class BalancesNormalizer implements NormalizerInterface
{

    /**
     * @param object $object
     * @param null $format
     * @param array $context
     * @return array|\Symfony\Component\Serializer\Normalizer\scalar
     */
    public function normalize($object, $format = null, array $context = [])
    {

        return [
            'id' => $object->getBalanceId(),
            'user_id' => $object->getUserId(),
            'type' => $object->getBalanceType(),
            'balance' => $object->getBalanceAmount(),
            'currency' => $object->getCurrency(),
            'system' => $context['system'],
            'display_name' => $context['display_name']
        ];
    }

    /**
     * @param mixed $data
     * @param null $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof BalancesInterface;
    }
}