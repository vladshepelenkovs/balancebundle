<?php

namespace Moneyfge\BalanceBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\SerializationContext;
use Moneyfge\BalanceBundle\ArrayType\TransactionsType;
use Moneyfge\BalanceBundle\Helper\ConstraintHelper;
use Moneyfge\BalanceBundle\ArrayType\BalancesType;
use Moneyfge\BalanceBundle\ArrayType\GetBalancesType;
use Symfony\Component\HttpFoundation\Request;


class ServiceController extends FOSRestController
{

    /**
     * Helper trait for constraint list conversion
     */
    use ConstraintHelper;

    /**
     * @Rest\Get("/balances", name="bb_balances")
     * @Rest\View()
     * @param Request $request
     * @return array
     */
    public function getBalancesAction(Request $request)
    {
        $requestData = $request->query->all();
        $requestData['user_id'] = $request->query->getInt('user_id') ?: null;

        $violations = $this->get('balance.array_validator')
            ->validateRequest(new GetBalancesType(), $requestData);

        if (0 !== count($violations) && $requestData) {
            return $this->get('balance.api_response')
                ->createErrorResponse(400, [ $this->constraintViolationListToString($violations) ]);
        }

        $accounts = $this->get('balance.balance_service')
            ->getBalancesFromService(
                $requestData['user_id'],
                isset($requestData['balance_type']) ? $requestData['balance_type'] : null,
                isset($requestData['currency']) ? $requestData['currency'] : null
            );

        $context = new SerializationContext();
        $context->setGroups( $this->getParameter('balance.serialization_groups')['balances'] );
        $data = $this->get('jms_serializer')->toArray($accounts, $context);

        $balancesViolations = $this->get('balance.array_validator')
            ->validateRequest(new BalancesType(), $data);

        if (0 !== count($balancesViolations)) {
            return $this->get('balance.api_response')
                ->createErrorResponse(400 , [ $this->constraintViolationListToString($violations) ]);
        }


        return $this->get('balance.api_response')
            ->createSuccessResponse(200, ['wallets' => $data]);
    }

    /**
     * @Rest\Post("/transactions", name="bb_transactions")
     * @Rest\View()
     * @param Request $request
     * @return array
     */
    public function postTransactionsAction(Request $request)
    {
        $requestData = $request->request->all();
        $transactionService = $this->get('balance.transaction_service');

        $violations = $this->get('balance.array_validator')
            ->validateRequest(new TransactionsType(), $requestData);

        if (0 !== count($violations)) {
            return $this->get('balance.api_response')
                ->createErrorResponse(400, [ $this->constraintViolationListToString($violations) ]);
        }

        $result = $transactionService->handleTransactions($requestData);

        if (false === $result) {
            return $this->get('balance.api_response')
                ->createErrorResponse(400, ['Error on writing to database']);
        }

        return $this->get('balance.api_response')
            ->createSuccessResponse(200, $result);
    }

}