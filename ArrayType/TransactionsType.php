<?php

namespace Moneyfge\BalanceBundle\ArrayType;

use Moneyfge\BalanceBundle\BundleInterface\ArrayTypeInterface;
use Symfony\Component\Validator\Constraints as Assert;

class TransactionsType implements ArrayTypeInterface
{
    public function getValidationRules()
    {
        return new Assert\All([
            new Assert\Collection([
                'balance_id' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'integer'])
                ],
                'balance_type' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'string'])
                ],
                'amount' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'numeric'])
                ],
                'transaction_id' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'string'])
                ],
                'currency' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'string'])
                ],
                'type' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'integer'])
                ],
                'status' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'integer'])
                ],
                'user_id' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'integer'])
                ]
            ])
        ]);
    }
}