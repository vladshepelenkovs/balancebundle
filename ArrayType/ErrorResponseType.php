<?php

namespace Moneyfge\BalanceBundle\ArrayType;

use Moneyfge\BalanceBundle\BundleInterface\ArrayTypeInterface;
use Symfony\Component\Validator\Constraints as Assert;

class ErrorResponseType implements ArrayTypeInterface
{

    public function getValidationRules()
    {
        return new Assert\All([
            'constraints' => array(
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'string']),
            ),
        ]);

    }


}
