<?php

namespace Moneyfge\BalanceBundle\ArrayType;

use Moneyfge\BalanceBundle\BundleInterface\ArrayTypeInterface;
use Symfony\Component\Validator\Constraints as Assert;

class TransactionResponseType implements ArrayTypeInterface
{

    public function getValidationRules()
    {
        return new Assert\Collection([
            'transaction_id' => [
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'string'])
            ],
            'rate' => [
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'numeric'])
            ]
        ]);
    }

}