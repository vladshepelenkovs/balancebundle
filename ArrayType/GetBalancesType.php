<?php

namespace Moneyfge\BalanceBundle\ArrayType;

use Moneyfge\BalanceBundle\BundleInterface\ArrayTypeInterface;
use Symfony\Component\Validator\Constraints as Assert;

class GetBalancesType implements ArrayTypeInterface
{

    public function getValidationRules()
    {
        return new Assert\Collection([
            'user_id' => [
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'integer'])
            ],
            'balance_type' => new Assert\Optional([
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'string'])
            ]),
            'currency' => new Assert\Optional([
                new Assert\Type(['type' => 'array'])
            ])
        ]);
    }

}