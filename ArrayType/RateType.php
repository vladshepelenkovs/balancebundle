<?php

namespace Moneyfge\BalanceBundle\ArrayType;

use Moneyfge\BalanceBundle\BundleInterface\ArrayTypeInterface;
use Symfony\Component\Validator\Constraints as Assert;

class RateType implements ArrayTypeInterface
{

    public function getValidationRules()
    {
        return new Assert\Collection([
            'currency_from' => new Assert\Optional([
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'string'])
            ]),
            'currency_to' => new Assert\Optional([
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'string'])
            ]),
        ]);

    }

}