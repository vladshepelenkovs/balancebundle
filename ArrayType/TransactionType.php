<?php

namespace Moneyfge\BalanceBundle\ArrayType;

use Moneyfge\BalanceBundle\BundleInterface\ArrayTypeInterface;
use Symfony\Component\Validator\Constraints as Assert;

class TransactionType implements ArrayTypeInterface
{

    public function getValidationRules()
    {
        return new Assert\Collection([
            'type' => [
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'integer'])
            ],
            'user_id' => [
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'integer'])
            ],
            'balance_type_from' => [
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'string'])
            ],
            'balance_type_to' => [
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'string'])
            ],
            'system_from' => [
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'string'])
            ],
            'system_to' => [
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'string'])
            ],
            'balance_id_from' => [
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'integer'])
            ],
            'balance_id_to' => [
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'integer'])
            ],
            'amount' => [
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'numeric'])
            ],
            'currency_from' => [
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'string'])
            ],
            'currency_to' => [
                new Assert\NotBlank(),
                new Assert\Type(['type' => 'string'])
            ],
        ]);
    }

}