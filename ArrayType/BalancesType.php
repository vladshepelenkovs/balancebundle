<?php

namespace Moneyfge\BalanceBundle\ArrayType;

use Moneyfge\BalanceBundle\BundleInterface\ArrayTypeInterface;
use Symfony\Component\Validator\Constraints as Assert;

class BalancesType implements ArrayTypeInterface
{

    public function getValidationRules()
    {
        return new Assert\All([
            new Assert\Collection([
                'id' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'integer'])
                ],
                'user_id' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'integer'])
                ],
                'balance' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'numeric'])
                ],
                'currency' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'string'])
                ],
                'type' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'string'])
                ],
                'system' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'string'])
                ],
                'display_name' => [
                    new Assert\NotBlank(),
                    new Assert\Type(['type' => 'string'])
                ],
            ])
        ]);

    }

}